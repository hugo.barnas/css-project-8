# css-project-8

![Capture_d_écran_2021-12-29_à_18.28.16](/uploads/5913443724e84ab3eaa7e92fa1355252/Capture_d_écran_2021-12-29_à_18.28.16.png)

![Capture_d_écran_2021-12-29_à_18.26.44](/uploads/deb3bd402068a23a50b8ea062395f62b/Capture_d_écran_2021-12-29_à_18.26.44.png)

## Objective

Improve my css expertise

## Technology used

- VSC
- html
- css
- GitLab

## Description

Build a responsive coming soon page for a webside 

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
It's a free project


